console.log("this is javascript es6 course ");

const list = [1,2,3,4,5];

for (let i=0;i<list.length;i++){
	console.log(i);
}

const name = "sohan";
console.log(`hello this is my ${name}`);

function getBook(author,title){
	return{
		author,
		title
	}
}

var book = getBook("sohan","mashud rana");
console.log(book);

function sayName(){
	console.log("my name is sohan");
}
sayName();

const sayLocation = location=>{
	console.log(`my location is in ${location}`);
}

sayLocation('paris');

//array function

var user ={
	name : "sohan",
	age :25,
	sayName: function(){
		console.log(`my name is ${this.name}`);
		const fullName = () => {
			console.log(`my full name is ${this.name} khan and my age is ${this.age}`);
		}
		fullName();
	}
}
user.sayName();


//forEach

const myList = ["sohan","sagor",'shormi']
myList.forEach((item,index)=>{
	return console.log(item,index);

});

const newList = myList.map(item =>item + "new");

console.log(newList);



function Person(name,age,color){
	this.name = name;
	this.age = age ;
	this.color = color;

}
const person1 = new Person("sohan",26,"blue");
console.log(person1.name,person1.age);